#!/bin/bash
#export FENICS_PREFIX=/home/fenics/build
#export FENICS_HOME=/home/fenics/local
export LD_LIBRARY_PATH=$FENICS_PREFIX:$FENICS_PREFIX/lib:/usr/local/lib:${LD_LIBRARY_PATH}
source $FENICS_HOME/fenics.env.conf
export SRC_DIR=$FENICS_HOME/src
export FENICS_PYTHON_MAJOR_VERSION=3
mkdir -p $SRC_DIR

# Required for Optizelle
export PYTHONPATH=$PYTHONPATH:$FENICS_PREFIX/share/optizelle/python/

pull_ipopt () {
    echo "FENICS-BUILDER: Updating ipopt..."
    cd $SRC_DIR
    rm -fR ipopt
    mkdir ipopt
    cd ipopt
    curl -O https://www.coin-or.org/download/source/Ipopt/Ipopt-${IPOPT_VER}.tgz
    tar -xvf Ipopt-${IPOPT_VER}.tgz
    cd Ipopt-$IPOPT_VER
    cd ThirdParty/Metis
    ./get.Metis
}

build_ipopt () {
    echo "FENICS-BUILDER: Building ipopt..."
    # install ipopt with metis and mumps, still need HSL :
    cd $SRC_DIR/ipopt/Ipopt-$IPOPT_VER

    # Fix compilation for parallel MPI versions
    #sed -i "s/#define MPI_COMM_WORLD IPOPT_MPI_COMM_WORLD//g" Ipopt/src/Algorithm/LinearSolvers/IpMumpsSolverInterface.cpp
    #sed -i "s/MPI_COMM_WORLD/MPI_COMM_SELF/g" Ipopt/src/Algorithm/LinearSolvers/IpMumpsSolverInterface.cpp

    ./configure --with-blas="-lblas -llapack" --with-lapack="-llapack" --prefix="${FENICS_PREFIX}"  --enable-debug --enable-shared --with-mumps-incdir="/usr/local/petsc-32/include  -I/usr/include/mpi" --with-mumps-lib="/usr/local/petsc-32/lib"
    make -j install
}
update_ipopt () {
    pull_ipopt
    build_ipopt
}
update_pyipopt () {
    cd $SRC_DIR
    git clone https://github.com/pf4d/pyipopt.git
    cd pyipopt
    sed -i "s#/usr/local#${FENICS_PREFIX}#g" setup.py
    sed -i "s/coinmumps/dmumps/g" setup.py
    sed -i "s#library_dirs=\[IPOPT_LIB\]#library_dirs=[IPOPT_LIB,'/usr/local/petsc-32/lib']#g" setup.py
    python${FENICS_PYTHON_MAJOR_VERSION} setup.py build
    python${FENICS_PYTHON_MAJOR_VERSION} setup.py install --prefix="${FENICS_PREFIX}"
}

pull_optizelle () {
    echo "FENICS-BUILDER: Pulling optizelle..."
    cd $SRC_DIR
    if [ -d Optizelle ]
    then
	    cd Optizelle
        git pull
    else
	    git clone https://github.com/OptimoJoe/Optizelle.git
	    cd Optizelle
        git checkout master
    fi
}
build_optizelle () {
    echo "FENICS-BUILDER: Building optizelle..."
    if [ -d $SRC_DIR/Optizelle ]
    then
	cd $SRC_DIR/Optizelle
	    mkdir -p build
	cd build
	cmake ../ -DCMAKE_INSTALL_PREFIX=$FENICS_PREFIX -DJSONCPP_LIBRARY=/usr/lib/x86_64-linux-gnu/libjsoncpp.so -DENABLE_PYTHON=True -DPYTHON_EXECUTABLE:FILEPATH=/usr/bin/python${FENICS_PYTHON_MAJOR_VERSION}
	make
	make install
    else
	    echo "FENICS-BUILDER: optizelle cannot be found. Please run pull_optizelle first."
    fi
}
update_optizelle () {
    pull_optizelle
    build_optizelle
}
clean_up () {
    rm -rf $SRC_DIR
}
