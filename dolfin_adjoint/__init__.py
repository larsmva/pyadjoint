"""This just acts as an alias for fenics_adjoint.

"""
import pyadjoint
__version__ = pyadjoint.__version__

from fenics_adjoint import *
